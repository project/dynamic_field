<?php

/**
 * @file
 * Code Dynamic Field Widget.
 */

/**
 * Implements hook_field_widget_info().
 */
function dynamic_field_field_widget_info() {
  return array(
    'field_wysiwyg_dynamic_widget' => array(
      'label'       => t('Dynamic Field Widget'),
      'field types' => array('field_wysiwyg_dynamic'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 *
 * hook_widget_form() is where Drupal tells us to create form elements for
 * our field's widget.
 *
 * We provide one of three different forms, depending on the widget type of
 * the Form API item provided.
 */
function dynamic_field_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  return $element;
}

/**
 * var_dump($field['field_name']);die;
 * Implements hook_field_widget_error().
 *
 * hook_field_widget_error() lets us figure out what to do with errors
 * we might have generated in hook_field_validate(). Generally, we'll just
 * call form_error().
 *
 * @see form_error()
 */
function dynamic_field_field_widget_error($element, $error, $form, &$form_state) {
}

/**
 * Returns template settings.
 *
 * @param $settings_path
 *   path to widget file settings.
 *
 * @return array
 *   Array containing the widget settings.
 *
 * @throws \Exception
 */
function load_settings_file($settings_path) {
  if (!file_exists($settings_path)) {
    return [];
  }
  // Load SPYC library.
  $library = libraries_load('spyc');
  if ($library['loaded']) {
    try {
      $fields_data = Spyc::YAMLLoad($settings_path) ?: [];
      return $fields_data;
    } catch (Exception $e) {
      watchdog_exception("The $settings_path contains invalid YAML", $e);
    }
  }
  else {
    drupal_set_message(t('Spyc library is required for parsing YMl, please Read Readme file Requirements and Installation sections, and retry please'), 'error');
    return [];
  }
}
