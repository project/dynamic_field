<?php

/**
 * @file
 * Code for Dynamic Field Type.
 */


/**
 * Implements hook_field_info().
 *
 * Provides the description of the field.
 */
function dynamic_field_field_info() {
  return array(
    // We name our field as the associative name of the array.
    'field_wysiwyg_dynamic' => array(
      'label'             => t('Dynamic Field'),
      'description'       => t('Dynamic Field.'),
      'default_widget'    => 'field_wysiwyg_dynamic_widget',
      'default_formatter' => 'field_wysiwyg_dynamic_formatter',
    ),
  );
}

/**
 * Implements hook_field_validate().
 */
function dynamic_field_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {

}

/**
 * Implements hook_field_is_empty().
 *
 * hook_field_is_empty() is where Drupal asks us if this field is empty.
 * Return TRUE if it does not contain data, FALSE if it does. This lets
 * the form API flag an error when required fields are empty.
 */
function dynamic_field_field_is_empty($item, $field) {
  return empty($item['data']);
}
