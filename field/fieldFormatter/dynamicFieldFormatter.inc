<?php

/**
 * @file
 * Code Dynamic Field Formatter.
 */

/**
 * Implements hook_field_formatter_info().
 *
 * We need to tell Drupal that we have a different types of formatters.
 *
 */
function dynamic_field_field_formatter_info() {
  return array(
    'field_wysiwyg_dynamic_formatter' => array(
      'label'       => t('Dynamic Field formatter'),
      'field types' => array('field_wysiwyg_dynamic'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function dynamic_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  return $element;
}
