<?php foreach ($values['fields'] as $item) : ?>
    <div class="fdb-block">
        <div class="row justify-content-center">
            <div class="col col-md-8 text-center">
                <h1><?php print $item['title'] ?></h1>
                <p class="text-h3"><?php print $item['description'] ?></p>
            </div>
        </div>
    </div>
<?php endforeach; ?>
