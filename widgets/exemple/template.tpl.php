<div class="bloc-chiffres my-3">

    <div class="bloc-chiffres__wrapper slider-on-mobile row justify-content-center pb-3">

        <?php foreach ($values['fields'] as $item) : ?>
        <?php $image_url = get_image($item['image'], $style = NULL); ?>
        <div class="col-3 d-flex flex-column chiffre-item">

            <div class="chiffre-item__img mx-auto d-block mb-1">
                <img src="<?php print $image_url ?>" <?php if ($item['image_alt']) : ?> alt="<?php print $item['image_alt'] ?>" <?php endif; ?> <?php if ($item['image_title']) : ?> title="<?php print $item['image_title'] ?>" <?php endif; ?>>
            </div>

            <h3 class="chiffre-item__description text-center"><?php print $item['description'] ?></h3>

        </div>
        <?php endforeach; ?>
    </div>

    <div class="d-flex justify-content-center mt-3 col-12 bloc-chiffres__cta">
        <?php print $values['extra_field']['cta'] ?>
    </div>

</div>
