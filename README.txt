CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Maintainers
 * Requirements
 * configuration

 INTRODUCTION
 ------------

 The Dynamic Field allows you to create widgets that will contain
 dynamically reassembled fields based on a settings.yml
 file linked to each widget.
 The module contains only one field that can be linked to any Drupal entity.

 INSTALLATION
 ------------

  * Install as you would normally install a contributed Drupal module. See:
    https://www.drupal.org/docs/7/extend/installing-modules
    for further information.

 REQUIREMENTS
 ------------

  This module need no requirements.

 CONFIGURATION
 -------------

 Maintainers
 --------

 Maintainers:
 * Oualid-EZR - https://www.drupal.org/u/oualid-ezr
 * Hamza Bahlaouane - https://www.drupal.org/u/hamza-bahlaouane
 * Berramou - https://www.drupal.org/u/berramou
